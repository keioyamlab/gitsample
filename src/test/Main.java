package test;

import static org.junit.Assert.*;

import org.junit.Test;

public class Main {

	@Test
	public void ミリ秒を入力したら日付を返してくれること() {
		long time = 1386744104;
		String expected = "2013-12-11 15:41:44";
		String actual = gagagagasu.Main.dateString(time);
		assertEquals(expected, actual);
	}

}

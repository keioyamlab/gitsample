package gagagagasu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		final String FILE_NAME = "loss_result.txt";; 
		
		ArrayList<String> idList = madeIDList();
		
		FileWriter fw = madeFileWriter(FILE_NAME);
		
		Statement st = madeStatement();
		calcFromMySQL(st,idList,fw);

		try {
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static ArrayList<String> madeIDList(){
		ArrayList<String> idList = new ArrayList<String>();

		idList.add("gsk001");
		idList.add("gsk002");
		idList.add("gsk003");
		idList.add("gsk004");
		idList.add("gsk005");
		idList.add("gsk006");
		idList.add("gsk007");
		idList.add("gsk008");
		idList.add("gsk009");
		idList.add("gsk010");

		return idList;

	}
	
	private static FileWriter madeFileWriter(String fileName){
		File file = new File(fileName);
		
		try {
			
			if(file.createNewFile()){
				System.out.println("ファイルの新規作成"+fileName);
			}
			FileWriter fw = new FileWriter(file,true);
			
			return fw;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	private static Statement madeStatement(){
		String servername = "localhost";
		String urlprefix = "jdbc:mysql://";
		String databasename = "nagasakidata";
		String user = "root";
		String password = "root";
		String serverencoding = "UTF-8";

		String url = urlprefix + servername + "/" + databasename + "?useUnicode=true&characterEncoding=" + serverencoding;

		System.out.println("Connected....");

		try {

			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection(url, user, password);
			Statement st = con.createStatement();

			System.out.println("コネクト成功");

			return st;

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	private static void calcFromMySQL(Statement st,ArrayList<String> idList,FileWriter fw){
		String tableName = "nagasaki_pp_c";
		String kindOfCollect = "recordTimestamp";

		try {
			for (int i = 0; i < idList.size(); i++) {
				String id = idList.get(i);
				String sql = "SELECT recordTimestamp FROM "+tableName+" WHERE name = '"+id+"'";
				ResultSet result = st.executeQuery(sql);
				long beforeTime = 0;
				int j = 0;
				
				while(result.next()){
					long time = result.getLong(kindOfCollect);
					
					if(beforeTime!=0){
						calclate(id,time, beforeTime,fw);
					}
					
					beforeTime = time;
					j++;
				}
				
				System.out.println(id+":"+j+"個のデータを検出");
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private static void calclate(String id, long time, long beforeTime, FileWriter fw){
		final int THRESHOLD = 300;
		long margine = time - beforeTime;
		
		if(margine>=THRESHOLD){
			write2File(id , time , beforeTime , fw);
		}
		
	}
	
	private static void write2File(String id , long time , long beforeTime, FileWriter fw){
		String line = formatedString(id,time,beforeTime);
		line += "\r\n";
		
		try {
			
			fw.write(line);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static String formatedString(String id, long time, long beforeTime){
		String date1 = dateString(time);
		String date2 = dateString(beforeTime);
		long margine = time - beforeTime;
		String line = id + ",";
		line += date2 + "," + date1 + ",";
		
		long hour = margine/3600;
		long minute = (margine-(3600*hour))/60;
		long second = margine - (3600 * hour) - (60 * minute);
		
		String total = "";
		
		if(hour>0){
			total += hour + "時間";
		}
		if(minute>0){
			total += minute + "分";
		}
		if(second>0){
			total += second + "秒";
		}
		
		line +=total;
		
		return line;
	}
	final static String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
	public static String dateString(long time){
		String date;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
		date = sdf.format(new Date(time*1000));
		return date;
	}
	

}
